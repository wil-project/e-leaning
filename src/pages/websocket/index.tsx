import SocketClient from "@/components/WebSocket/client";
import React from "react";

function WebSocket() {
  return (
    <>
      <div>WebSocket</div>
      <SocketClient />
    </>
  );
}

export default WebSocket;
